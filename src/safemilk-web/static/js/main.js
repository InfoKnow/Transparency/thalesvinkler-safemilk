function show_modal(modal='#myModal'){
	$(modal).modal('show');
}

function hide_modal(modal='#myModal'){
	$(modal).modal('hide');
}

function send_ajax(url, type, dataType){

    $.ajax({
        type    : type,
        url     : url,
        dataType: dataType,
        success     : function(data){
            if(data['html_replace_tag'] !== 'undefined'){
                $(data['html_replace_tag']).html(data['data']);
                $.lastCall = (this.url);
            }

        },
        error       : function (xhr, ajaxOptions, thrownError) {
            $.lastCall = (this.url);
        }
    });

}

function send_ajax_and_show_modal(url, type, dataType){

    $.ajax({
        type    : type,
        url     : url,
        dataType: dataType,
        success     : function(data){
            if(data['html_replace_tag'] !== 'undefined'){
                $(data['html_replace_tag']).html(data['data']);
                $.lastCall = (this.url);
                $('#myModal').html(data['data']);
                show_modal();
            }

        },
        error       : function (xhr, ajaxOptions, thrownError) {
            $.lastCall = (this.url);
        }
    });

}

$(document).ready(function(){

    alert();
	
	$('.btn-show-modal').on('click', function(){
		var url = $(this).attr('data-url');
		send_ajax_and_show_modal(url, 'get', 'json');
	});

});