from django.conf.urls import url
from django.contrib.auth import views as auth_views
from admin import views


app_name = 'admin'
urlpatterns = [    
    url(r'^$', views.index, name='index'),

    # Login/Logout
    url(r'^login/$', auth_views.login,{'template_name': 'account/login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout_then_login, name='logout'),

    # Cadastro
    url(r'cadastro/usuario$', views.register_user, name="register_user"),
    url(r'cadastro/laticinio/(?P<pk>[0-9]+)/$', views.register_laticinio, name="register_laticinio"),

    # Usuários
    url(r'usuarios/$', views.list_usuarios, name="list_usuarios"),
    url(r'usuario/$', views.create_usuario, name='create_usuario'),
    url(r'usaurio/(?P<pk>[0-9]+)/$', views.AdminUpdate.as_view(), name='update_usuario'),
    url(r'usaurio/delete_usuario/(?P<pk>[0-9]+)/$', views.delete_usuario, name='delete_usuario'),

    # Localização
    url(r'^locais/$', views.list_locais, name='list_locais'),
    url(r'^local/$', views.create_local, name='create_local'),
    url(r'^local/(?P<pk>[0-9]+)$', views.LocalUpdate.as_view(), name='update_local'),
    url(r'^local/delete_local/(?P<id>[0-9]+)$', views.delete_local, name='delete_local'),    

    # Laticinio
    url(r'^laticinios/$', views.list_laticinios, name='list_laticinios'),
    url(r'^laticinio/$', views.create_laticinio, name='create_laticinio'),
    url(r'^laticinio/(?P<pk>[0-9]+)$', views.LaticinioUpdate.as_view(), name='update_laticinio'),
    url(r'^laticinio/delete_laticinio/(?P<id>[0-9]+)$', views.delete_laticinio, name='delete_laticinio'),
    # url(r'^laticinios/mostrar_empresa/(?P<id>[0-9]+)$', views.mostrar_empresa, name='mostrar_laticinio'),    

    # Entreposto
    url(r'^entrepostos/$', views.list_entrepostos, name='list_entrepostos'),
    url(r'^entreposto/$', views.create_entreposto, name='create_entreposto'),
    url(r'^entreposto/(?P<pk>[0-9]+)$', views.EntrepostoUpdate.as_view(), name='update_entreposto'),
    url(r'^entreposto/delete_entreposto/(?P<id>[0-9]+)$', views.delete_entreposto, name='delete_entreposto'),

    # Fazenda
    url(r'^fazendas/$', views.list_fazendas, name='list_fazendas'),
    url(r'^fazenda/$', views.create_fazenda, name='create_fazenda'),
    url(r'^fazenda/(?P<pk>[0-9]+)$', views.FazendaUpdate.as_view(), name='update_fazenda'),
    url(r'^fazenda/delete_fazenda/(?P<id>[0-9]+)$', views.delete_fazenda, name='delete_fazenda'),

    # Ponto
    url(r'^pontos/$', views.list_pontos, name='list_pontos'),
    url(r'^ponto/$', views.create_ponto, name='create_ponto'),
    url(r'^ponto/(?P<pk>[0-9]+)$', views.PontoUpdate.as_view(), name='update_ponto'),
    url(r'^ponto/delete_ponto/(?P<id>[0-9]+)$', views.delete_ponto, name='delete_ponto'),

    # Linha
    url(r'^linhas/$', views.list_linhas, name='list_linhas'),
    url(r'^linha/$', views.create_linha, name='create_linha'),
    url(r'^linha/(?P<pk>[0-9]+)$', views.LinhaUpdate.as_view(), name='update_linha'),
    url(r'^linha/delete_linha/(?P<id>[0-9]+)$', views.delete_linha, name='delete_linha'),

    # Caminhão
    url(r'^caminhoes/$', views.list_caminhoes, name='list_caminhoes'),
    url(r'^caminhao/$', views.create_caminhao, name='create_caminhao'),
    url(r'^caminhao/(?P<pk>[0-9]+)$', views.CaminhaoUpdate.as_view(), name='update_caminhao'),
    url(r'^caminhao/delete_caminhao/(?P<id>[0-9]+)$', views.delete_caminhao, name='delete_caminhao'),    

    # Cor
    url(r'^cores/$', views.list_cores, name='list_cores'),
    url(r'^cor/$', views.create_cor, name='create_cor'),
    url(r'^cor/(?P<pk>[0-9]+)$', views.CorUpdate.as_view(), name='update_cor'),
    url(r'^cor/delete_cor/(?P<id>[0-9]+)$', views.delete_cor, name='delete_cor'),

    # Teste
    url(r'^testes/$', views.list_testes, name='list_testes'),
    url(r'^teste/$', views.create_teste, name='create_teste'),
    url(r'^teste/(?P<pk>[0-9]+)$', views.TesteUpdate.as_view(), name='update_teste'),
    url(r'^teste/delete_teste/(?P<id>[0-9]+)$', views.delete_teste, name='delete_teste'),












    url(r'^save_dados_gerais/$', views.save_dados_gerais, name='save_dados_gerais'),
    url(r'^save_area_atuacao/$', views.save_area_atuacao, name='save_area_atuacao'),
    url(r'^remove_area_atuacao/(?P<id>[0-9]+)$', views.remove_area_atuacao, name='remove_area_atuacao'),
    url(r'^save_servico/$', views.save_servico, name='save_servico'),
    url(r'^remove_servico/(?P<id>[0-9]+)$', views.remove_servico, name='remove_servico'),

    # Empresas
    url(r'^empresas/$', views.empresas, name='empresas'),
    url(r'^empresa/(?P<id>[0-9]+)$', views.empresa, name='empresa'),
    url(r'^empresa/$', views.empresa, name='empresa'),
    url(r'^empresas/delete_empresa/(?P<id>[0-9]+)$', views.delete_empresa, name='delete_empresa'),
    url(r'^empresas/mostrar_empresa/(?P<id>[0-9]+)$', views.mostrar_empresa, name='mostrar_empresa'),

    # Areas de Atuacao
    url(r'^areas_atuacao/$', views.areas_atuacao, name='areas_atuacao'),

    # Serviços
    url(r'^servicos/$', views.servicos, name='servicos'),

    # Cases
    url(r'cases/$', views.cases, name='cases'),    
    url(r'cases/novo$', views.novo_case, name='novo_case'), 
    url(r'cases/(?P<pk>[0-9]+)/$', views.CaseUpdate.as_view(), name='case-update'),
    url(r'cases/(?P<pk>[0-9]+)/delete$', views.delete_case, name='case-delete'),
    url(r'cases/mostrar_case/(?P<pk>[0-9]+)$', views.mostrar_case, name='mostrar-case'),

    # Necessidades Serviços
    url(r'servicos_solicitados/$', views.servicos_solicitados, name='servicos_solicitados'),
    url(r'servicos_solicitados/(?P<pk>[0-9]+)$', views.servico_solicitado, name='servicos-solicitados-details'),

    # Feedbacks
    url(r'feedbacks/$', views.feedbacks, name="feedbacks"),
    url(r'feedbacks/(?P<pk>[0-9]+)$', views.feedback, name='feedback-detail'),


    # url(r'^login', views.login, name='login')
]
