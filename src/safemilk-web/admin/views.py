from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.edit import UpdateView, DeleteView, FormView
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import render, redirect
from django.template.loader import get_template
from django.template import Context
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.admin.views.decorators import staff_member_required
from admin.forms import (CadastroLocalForm,
                         CadastroLaticinioForm, CadastroEntrepostoForm,
                         CadastroFazendaForm, CadastroLinhaForm, 
                         CadastroPontoForm, CadastroCaminhaoForm,
                         CadastroTesteForm, CadastroCorForm, 
                         DadosEmpresaForm, SaveAreaAtuacaoForm,
                         SaveServicoForm, CadastroEmpresaForm,
                         CadastroCaseForm, CadastroUsuarioForm)

from institucional.models import (AreaAtuacao, Servico, EmpresaJunior,
                                  Case, NecessidadesServico, Feedback)
from logistic.models import Local, Laticinio, Entreposto, Fazenda, Ponto, Linha, Caminhao
from colortest.models import Cor, Teste

from rest_framework import viewsets, status
from rest_framework.decorators import api_view
from rest_framework.response import Response



# Create your views here.

# Index
@login_required
def index(request):
    user = request.user
    # Super Usuários
    if user.is_superuser:
        laticinios = Laticinio.objects.all().order_by('pk')
        # empresas_apresentadas = [e for e in empresas if e.mostrar == True]
        linhas = Linha.objects.all().order_by('pk')
        linhas_ativas = [l for l in linhas if l.ativa == True]
        pontos = Ponto.objects.all().order_by('pk')
        testes = Teste.objects.all().order_by('pk')
        testes_contaminados = [t for t in testes if t.resultado == 'Contaminado']
        return render(request, "staff/index.html", {'laticinios': laticinios,
                                                    'linhas': linhas,
                                                    'linhas_ativas': linhas_ativas,
                                                    'pontos': pontos,
                                                    'testes': testes,
                                                    'testes_contaminados': testes_contaminados,
                                                    })
    else:
        # Proprietário/Técnico/Fiscal
        if user.is_staff:
            laticinios = Laticinio.objects.all().order_by('pk')
            # empresas_apresentadas = [e for e in empresas if e.mostrar == True]
            linhas = Linha.objects.all().order_by('pk')
            linhas_ativas = [l for l in linhas if l.ativa == True]
            pontos = Ponto.objects.all().order_by('pk')
            testes = Teste.objects.all().order_by('pk')
            testes_contaminados = [t for t in testes if t.resultado == 'Contaminado']
            return render(request, "staff/index.html", {'laticinios': laticinios,
                                                        'linhas': linhas,
                                                        'linhas_ativas': linhas_ativas,
                                                        'pontos': pontos,
                                                        'testes': testes,
                                                        'testes_contaminados': testes_contaminados,
                                                        })
        else:
            # Caminhoneiro
            # areas_atuacao = [ a.nome for a in AreaAtuacao.objects.all() ]
            # servicos = [s.nome for s in Servico.objects.all()]
            # area_atuacao_form = SaveAreaAtuacaoForm()
            # servico_form = SaveServicoForm()
            # if user.empresajunior is not None:
            #     form = DadosEmpresaForm(initial=user.empresajunior.__dict__)
            # else:
            #     form = DadosEmpresaForm()
            return render(request, "caminhoneiro/index.html")

# Cadastro Views
def register_user(request):

    if request.method == 'GET':
        form = CadastroUsuarioForm()
        return render(request, 'account/form_user.html', {'form': form})
    
    if request.method == 'POST':
        form = CadastroUsuarioForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_staff = True
            user.save()
            return redirect('admin:register_laticinio', pk=user.id)
        else:
            return render(request, 'account/form_user.html', {  'form': form})

def register_laticinio(request, pk=None):

    if request.method == 'GET':
        form = CadastroLaticinioForm()
        return render(request,'account/form_laticinio.html', {  'form': form,
                                                                'pk': pk
                                                                })

    if request.method == 'POST':
        form = CadastroLaticinioForm(request.POST, request.FILES)
        print(form)
        if form.is_valid():
            print(form)
            form.save(request.user)
            return redirect('admin:index')
        print(form.errors)
        return render(request,'account/form_laticinio.html', {'form': form})


# Usuários Views
@staff_member_required
def list_usuarios(request):
    user = request.user

    if user.is_superuser:
        usuarios = User.objects.filter(is_staff=True).order_by('pk')
        return render(request, 'staff/usuarios/list.html', {'usuarios': usuarios})
    else:
        usuarios = User.objects.filter(is_staff=False).order_by('pk')
        return render(request, 'staff/usuarios/list.html', {'usuarios': usuarios})

@staff_member_required
def create_usuario(request):

    user = request.user

    if user.is_superuser:
        if request.method == 'GET':
            form = CadastroUsuarioForm()
            return render(request, 'staff/usuarios/form.html', {'form': form})
        
        if request.method == 'POST':
            form = CadastroUsuarioForm(request.POST)
            if form.is_valid():
                user = form.save(commit=False)
                user.is_staff = True
                user.save()
                return redirect('admin:list_usuarios')
            else:
                return render(request, 'staff/usuarios/form.html', {'form': form})
    else:
        if user.is_staff:
            if request.method == 'GET':
                form = CadastroUsuarioForm()
                return render(request, 'staff/usuarios/form.html', {'form': form})
        
            if request.method == 'POST':
                form = CadastroUsuarioForm(request.POST)
                if form.is_valid():
                    user = form.save(commit=False)
                    user.is_staff = False
                    user.save()
                    return redirect('admin:list_usuarios')
                else:
                    return render(request, 'staff/usuarios/form.html', {'form': form})
        else:
            return render(request, 'staff/usuarios/form.html')


class AdminUpdate(UpdateView):
    template_name = 'staff/usuarios/form.html'
    model = User
    fields = '__all__'


@staff_member_required
def delete_usuario(request, pk):

    if request.method == "GET":
        admins = User.objects.filter(is_staff=True)
        if len(admins) > 1:
            admin = User.objects.get(pk=pk)
            admin.delete()
        
        return redirect('admin:usuarios')

# Localizacao Views
@staff_member_required
def list_locais(request):
    locais = Local.objects.all().order_by('pk')
    return render(request, 'staff/locais/list.html', {'locais': locais})

@staff_member_required
def create_local(request):

    if request.method == 'GET':
        form = CadastroLocalForm()
        return render(request,'staff/locais/form.html', {'form': form})

    if request.method == 'POST':
        form = CadastroLocalForm(request.POST, request.FILES)
        if form.is_valid():
            form.save(request.user)
            return redirect('admin:list_locais')
        print(form.errors)
        return render(request,'staff/locais/form.html', {'form': form})

class LocalUpdate(UpdateView):
    template_name = 'staff/locais/form.html'
    model = Local
    fields = '__all__'

@staff_member_required
def delete_local(request, id):

    if request.method == "GET":
        local = Local.objects.get(pk=id)
        # user = local.user
        local.delete()
        # user.delete()

        return redirect('admin:list_locais')

# Laticinio Views
@staff_member_required
def list_laticinios(request):
    laticinios = Laticinio.objects.all().order_by('pk')
    return render(request, 'staff/laticinios/list.html', {'laticinios': laticinios})

@staff_member_required
def create_laticinio(request):

    if request.method == 'GET':
        form = CadastroLaticinioForm()
        return render(request,'staff/laticinios/form.html', {'form': form})

    if request.method == 'POST':
        form = CadastroLaticinioForm(request.POST, request.FILES)
        if form.is_valid():
            form.save(request.user)
            return redirect('admin:list_laticinios')
        print(form.errors)
        return render(request,'staff/laticinios/form.html', {'form': form})

class LaticinioUpdate(UpdateView):
    template_name = 'staff/laticinios/form.html'
    model = Laticinio
    fields = '__all__'

@staff_member_required
def delete_laticinio(request, id):

    if request.method == "GET":
        laticinio = Laticinio.objects.get(pk=id)
        # user = laticinio.user
        laticinio.delete()
        # user.delete()

        return redirect('admin:list_laticinios')

# Entreposto Views
@staff_member_required
def list_entrepostos(request):
    entrepostos = Entreposto.objects.all().order_by('pk')
    return render(request, 'staff/entrepostos/list.html', {'entrepostos': entrepostos})

@staff_member_required
def create_entreposto(request):

    if request.method == 'GET':
        form = CadastroEntrepostoForm()
        return render(request,'staff/entrepostos/form.html', {'form': form})

    if request.method == 'POST':
        form = CadastroEntrepostoForm(request.POST, request.FILES)
        if form.is_valid():
            form.save(request.user)
            return redirect('admin:list_entrepostos')
        print(form.errors)
        return render(request,'staff/entrepostos/form.html', {'form': form})

class EntrepostoUpdate(UpdateView):
    template_name = 'staff/entrepostos/form.html'
    model = Entreposto
    fields = '__all__'

@staff_member_required
def delete_entreposto(request, id):

    if request.method == "GET":
        entreposto = Entreposto.objects.get(pk=id)
        # user = entreposto.user
        entreposto.delete()
        # user.delete()

        return redirect('admin:list_entrepostos')

# Fazenda Views
@staff_member_required
def list_fazendas(request):
    fazendas = Fazenda.objects.all().order_by('pk')
    return render(request, 'staff/fazendas/list.html', {'fazendas': fazendas})

@staff_member_required
def create_fazenda(request):

    if request.method == 'GET':
        form = CadastroFazendaForm()
        return render(request,'staff/fazendas/form.html', {'form': form})

    if request.method == 'POST':
        form = CadastroFazendaForm(request.POST, request.FILES)
        if form.is_valid():
            form.save(request.user)
            return redirect('admin:list_fazendas')
        print(form.errors)
        return render(request,'staff/fazendas/form.html', {'form': form})

class FazendaUpdate(UpdateView):
    template_name = 'staff/fazendas/form.html'
    model = Fazenda
    fields = '__all__'

@staff_member_required
def delete_fazenda(request, id):

    if request.method == "GET":
        fazenda = Fazenda.objects.get(pk=id)
        # user = fazenda.user
        fazenda.delete()
        # user.delete()

        return redirect('admin:list_fazendas')

# Ponto Views
@login_required
def list_pontos(request):
    pontos = Ponto.objects.all().order_by('pk')
    return render(request, 'staff/pontos/list.html', {'pontos': pontos})

@login_required
def create_ponto(request):

    if request.method == 'GET':
        form = CadastroPontoForm()
        return render(request,'staff/pontos/form.html', {'form': form})

    if request.method == 'POST':
        form = CadastroPontoForm(request.POST, request.FILES)
        if form.is_valid():
            form.save(request.user)
            return redirect('admin:list_pontos')
        print(form.errors)
        return render(request,'staff/pontos/form.html', {'form': form})

class PontoUpdate(UpdateView):
    template_name = 'staff/pontos/form.html'
    model = Ponto
    fields = '__all__'

@login_required
def delete_ponto(request, id):

    if request.method == "GET":
        ponto = Ponto.objects.get(pk=id)
        # user = ponto.user
        ponto.delete()
        # user.delete()

        return redirect('admin:list_testes')

# Linha Views
@login_required
def list_linhas(request):
    linhas = Linha.objects.all().order_by('pk')
    return render(request, 'staff/linhas/list.html', {'linhas': linhas})

@login_required
def create_linha(request):

    if request.method == 'GET':
        form = CadastroLinhaForm()
        return render(request,'staff/linhas/form.html', {'form': form})

    if request.method == 'POST':
        form = CadastroLinhaForm(request.POST, request.FILES)
        if form.is_valid():
            form.save(request.user)
            return redirect('admin:list_linhas')
        print(form.errors)
        return render(request,'staff/linhas/form.html', {'form': form})

class LinhaUpdate(UpdateView):
    template_name = 'staff/linhas/form.html'
    model = Linha
    fields = '__all__'

@login_required
def delete_linha(request, id):

    if request.method == "GET":
        linha = Linha.objects.get(pk=id)
        # user = linha.user
        linha.delete()
        # user.delete()

        return redirect('admin:list_linhas')

# Caminhão Views
@staff_member_required
def list_caminhoes(request):
    caminhoes = Caminhao.objects.all().order_by('pk')
    return render(request, 'staff/caminhoes/list.html', {'caminhoes': caminhoes})

@staff_member_required
def create_caminhao(request):

    if request.method == 'GET':
        form = CadastroCaminhaoForm()
        return render(request,'staff/caminhoes/form.html', {'form': form})

    if request.method == 'POST':
        form = CadastroCaminhaoForm(request.POST, request.FILES)
        if form.is_valid():
            form.save(request.user)
            return redirect('admin:list_caminhoes')
        print(form.errors)
        return render(request,'staff/caminhoes/form.html', {'form': form})

class CaminhaoUpdate(UpdateView):
    template_name = 'staff/caminhoes/form.html'
    model = Caminhao
    fields = '__all__'

@staff_member_required
def delete_caminhao(request, id):

    if request.method == "GET":
        caminhao = Caminhao.objects.get(pk=id)
        # user = caminhao.user
        caminhao.delete()
        # user.delete()

        return redirect('admin:list_caminhoes')

# Cor Views
@staff_member_required
def list_cores(request):
    cores = Cor.objects.all().order_by('pk')
    return render(request, 'staff/cores/list.html', {'cores': cores})

@staff_member_required
def create_cor(request):

    if request.method == 'GET':
        form = CadastroCorForm()
        return render(request,'staff/cores/form.html', {'form': form})

    if request.method == 'POST':
        form = CadastroCorForm(request.POST, request.FILES)
        if form.is_valid():
            form.save(request.user)
            return redirect('admin:list_cores')
        print(form.errors)
        return render(request,'staff/cores/form.html', {'form': form})

class CorUpdate(UpdateView):
    template_name = 'staff/cores/form.html'
    model = Cor
    fields = '__all__'

@staff_member_required
def delete_cor(request, id):

    if request.method == "GET":
        cor = Cor.objects.get(pk=id)
        # user = cor.user
        cor.delete()
        # user.delete()

        return redirect('admin:list_cores')

# Teste Views
@login_required
def list_testes(request):
    testes = Teste.objects.all().order_by('pk')
    return render(request, 'staff/testes/list.html', {'testes': testes})

@login_required
def create_teste(request):

    if request.method == 'GET':
        form = CadastroTesteForm()
        return render(request,'staff/testes/form.html', {'form': form})

    if request.method == 'POST':
        form = CadastroTesteForm(request.POST, request.FILES)
        if form.is_valid():
            form.save(request.user)
            return redirect('admin:list_testes')
        print(form.errors)
        return render(request,'staff/testes/form.html', {'form': form})

class TesteUpdate(UpdateView):
    template_name = 'staff/testes/form.html'
    model = Teste
    fields = '__all__'

@login_required
def delete_teste(request, id):

    if request.method == "GET":
        teste = Teste.objects.get(pk=id)
        # user = teste.user
        teste.delete()
        # user.delete()

        return redirect('admin:list_testes')



























@login_required
def save_dados_gerais(request):
    form = DadosEmpresaForm(request.POST, request.FILES, instance=request.user.empresajunior)
    if form.is_valid():
        form.save()
    else:
        user = request.user
        areas_atuacao = [ a.nome for a in AreaAtuacao.objects.all() ]
        servicos = [s.nome for s in Servico.objects.all()]
        area_atuacao_form = SaveAreaAtuacaoForm()
        servico_form = SaveServicoForm()
        return render(request, "ej/index.html", {'form':form, 
                                                 'user': user,
                                                 'empresa_junior': user.empresajunior,
                                                 'user_areas_atuacao': [a for a in user.empresajunior.areas_atuacao.all()],
                                                 'user_servicos': [s for s in user.empresajunior.servicos.all()],
                                                 'select_areas_atuacao': areas_atuacao,
                                                 'select_servico': servicos,
                                                 'area_atuacao_form': area_atuacao_form,
                                                 'servico_form': servico_form} )
    return redirect('admin:index')


@login_required
def save_area_atuacao(request):
    area_atuacao = AreaAtuacao.objects.filter(nome=request.POST.get("nome", )).first()
    if area_atuacao is None:
        area_atuacao = AreaAtuacao.objects.create(nome=request.POST.get("nome", ),
                                               descricao=request.POST.get("descricao", ))
    user = request.user
    user.empresajunior.areas_atuacao.add(area_atuacao)
    user.save()

    return redirect('admin:index')


@login_required
def remove_area_atuacao(request, id):
    user = request.user
    area_atuacao = AreaAtuacao.objects.get(pk=id)
    empresajunior = user.empresajunior
    empresajunior.areas_atuacao.remove(area_atuacao)
    for s in empresajunior.servicos.all():
        if area_atuacao.nome in [a.nome for a in s.areas_atuacao.all()]:
            empresajunior.servicos.remove(s)
    empresajunior.save()
    
    return redirect('admin:index')


@login_required
def save_servico(request):
    area_atuacao_id = request.POST.get("area_atuacao_number", )
    if request.POST.get("nome", ) is not None:
        novo_servico = Servico.objects.filter(nome=request.POST.get("nome", )).first()
        if novo_servico is None:
            novo_servico = Servico.objects.create(nome=request.POST.get("nome", ),
                                                  descricao=request.POST.get("descricao", ))
        area_atuacao = AreaAtuacao.objects.get(pk=area_atuacao_id)

        novo_servico.areas_atuacao.add(area_atuacao)

        novo_servico.save()
        user = request.user
        user.empresajunior.servicos.add(novo_servico)
        user.save()

    return redirect('admin:index')


@login_required
def remove_servico(request, id):
    user = request.user
    servico = Servico.objects.get(pk=id)
    empresajunior = user.empresajunior
    empresajunior.servicos.remove(servico)
    empresajunior.save()

    return redirect('admin:index')

@staff_member_required
def empresas(request):
    empresas = EmpresaJunior.objects.all().order_by('pk')
    return render(request, 'staff/empresas.html', {'empresas': empresas})

@staff_member_required
def empresa(request, id=None):

    if request.method == 'GET':

        form = CadastroEmpresaForm(initial={})
        empresa = None
        if id is not None:
            empresa = EmpresaJunior.objects.get(pk=id)

            if empresa is not None:
                form = CadastroEmpresaForm(initial={'nome': empresa.nome,
                                                    'username': empresa.user.username,
                                                    'email': empresa.user.email})   

        modal = get_template('core/modals/empresa_form.html')
        c = Context({'form': form, 'empresa': empresa})
        data = {'data': modal.render(c)}
        return JsonResponse(data)

    elif request.method == 'POST':
        form = CadastroEmpresaForm(request.POST)
        if form.is_valid():
            form.save()
            data = {'status': 200,
                    'message': 'operação feita com sucesso',
                    'hide_modal': True,
                    'after_url': reverse('admin:empresas')}
            return JsonResponse(data)
        else:
            print(form.errors)
            data = {'status': 504, 'erros': form.errors}
            return JsonResponse(data)

    else:
        data = {'status': 504, 'message': 'bad request'}
        return JsonResponse(data)


@staff_member_required
def delete_empresa(request, id):

    if request.method == "GET":
        empresa = EmpresaJunior.objects.get(pk=id)
        user = empresa.user
        empresa.delete()
        user.delete()

        return redirect('admin:empresas')


@staff_member_required
def mostrar_empresa(request, id):

    if request.method == "GET":
        empresa = EmpresaJunior.objects.get(pk=id)
        if empresa.mostrar == False:
            empresa.mostrar = True
        elif empresa.mostrar == True:
            empresa.mostrar = False
        empresa.save()

        return redirect('admin:empresas')


@staff_member_required
def areas_atuacao(request):
    areas_atuacao = AreaAtuacao.objects.all().order_by('pk')
    return render(request, 'staff/areas_atuacao.html', {'areas_atuacao': areas_atuacao})


@staff_member_required
def servicos(request):
    servicos = Servico.objects.all().order_by('pk')
    return render(request, 'staff/servicos.html', {'servicos': servicos})


@staff_member_required
def cases(request):
    cases = Case.objects.all().order_by('pk')
    return render(request, 'staff/cases/list.html', {'cases': cases})


@staff_member_required
def novo_case(request):

    if request.method == 'GET':
        form = CadastroCaseForm()
        return render(request,'staff/cases/form.html', {'form': form})

    if request.method == 'POST':
        form = CadastroCaseForm(request.POST, request.FILES)
        if form.is_valid():
            form.save(request.user)
            return redirect('admin:cases')
        print(form.errors)
        return render(request,'staff/cases/form.html', {'form': form})


class CaseUpdate(UpdateView):
    template_name = 'staff/cases/form.html'
    model = Case
    fields = '__all__'


@staff_member_required
def delete_case(request, pk):

    if request.method == "GET":
        case = Case.objects.get(pk=pk)
        case.delete()
        return redirect('admin:cases')


@staff_member_required
def mostrar_case(request, pk):

    if request.method == "GET":
        case = Case.objects.get(pk=pk)
        if case.apresentar == False:
            case.apresentar = True
        elif case.apresentar == True:
            case.apresentar = False
        case.save()

        return redirect('admin:cases')


@staff_member_required
def servicos_solicitados(request):
    servicos_solicitados = NecessidadesServico.objects.all().order_by('pk')
    return render(request, 'staff/servicos_solicitados/list.html', {'servicos_solicitados': servicos_solicitados})


@staff_member_required
def servico_solicitado(request, pk):
    servico_solicitado = NecessidadesServico.objects.get(pk=pk)
    return render(request, 'staff/servicos_solicitados/detail.html', {'servico_solicitado': servico_solicitado})


@staff_member_required
def feedbacks(request):
    feedbacks = Feedback.objects.all().order_by('pk')
    return render(request, 'staff/feedbacks/list.html', {'feedbacks': feedbacks})


@staff_member_required
def feedback(request, pk):
    feedback = Feedback.objects.get(pk=pk)
    return render(request, 'staff/feedbacks/detail.html', {'feedback': feedback})


