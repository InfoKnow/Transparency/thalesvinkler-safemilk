from django import forms
from django.utils.translation import gettext, gettext_lazy as _
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from institucional.models import EmpresaJunior, AreaAtuacao, Servico, Case
from logistic.models import Local, Laticinio, Entreposto, Fazenda, Ponto, Linha, Caminhao
from colortest.models import Cor, Teste

class CadastroUsuarioForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        fields = ('username', 'email')

class CadastroLocalForm(forms.ModelForm):

    class Meta:
        model = Local
        fields = '__all__'

class CadastroLaticinioForm(forms.ModelForm):

    class Meta:
        model = Laticinio
        fields = '__all__'


class CadastroEntrepostoForm(forms.ModelForm):

    class Meta:
        model = Entreposto
        fields = '__all__'


class CadastroFazendaForm(forms.ModelForm):

    class Meta:
        model = Fazenda
        fields = '__all__'

class CadastroPontoForm(forms.ModelForm):

    class Meta:
        model = Ponto
        fields = '__all__'

class CadastroLinhaForm(forms.ModelForm):
    # pontos = forms.ModelMultipleChoiceField(widgets=Select2MultipleWidget(), queryset=Company.objects.none())

    # pontos = forms.MultipleChoiceField(
    #     required=False,
    #     widget=forms.SelectMultiple,
    #     choices=[ p for p in Ponto.objects.all() ],
    # )
    class Meta:
        model = Linha
        fields = '__all__'
        # fields = ['nome', 'origem', 'origem', 'destino', 'laticinio', 'ativa', 'pontos']

class CadastroCaminhaoForm(forms.ModelForm):

    class Meta:
        model = Caminhao
        fields = '__all__'

class CadastroCorForm(forms.ModelForm):

    class Meta:
        model = Cor
        fields = '__all__'

class CadastroTesteForm(forms.ModelForm):

    class Meta:
        model = Teste
        fields = '__all__'





class CadastroEmpresaForm(UserCreationForm):
    nome = forms.CharField(required=True, max_length=100)

    class Meta(UserCreationForm.Meta):
        fields = ('username', 'email')

    def clean_nome(self):
        data = self.cleaned_data.get('nome')
        empresa = EmpresaJunior.objects.filter(nome=data)
        if len(empresa) != 0:
            raise forms.ValidationError(
                _('Empresa %(nome)s já está cadastrada'),
                code='invalid',
                params={'nome': data,}
            )
        return data

    def save(self):
        print(self.cleaned_data.get('username'))
        print(self.cleaned_data.get('email'))
        print(self.cleaned_data.get('password1'))
        print(self.cleaned_data.get('nome'))

        user = User(username=self.cleaned_data.get('username'),
                    email=self.cleaned_data.get('email'))
        user.set_password(self.cleaned_data.get('password1'))
        user.save()

        empresa = EmpresaJunior(nome=self.cleaned_data.get('nome'), user_id=user.id)
        empresa.save()

        user.empresajunior = empresa
        user.save()

        return 


class DadosEmpresaForm(forms.ModelForm):

    class Meta:
        model = EmpresaJunior
        fields = ['nome', 'nome_curto', 'logo', 'descricao_empresa',
                  'email_empresa', 'site', 'facebook']


class SaveAreaAtuacaoForm(forms.ModelForm):

    class Meta:
        model = AreaAtuacao
        fields = ['nome', 'descricao']


class SaveServicoForm(forms.ModelForm):

    class Meta:
        model = Servico
        fields = ['nome', 'descricao']


class CadastroCaseForm(forms.ModelForm):

    class Meta:
        model = Case
        fields = '__all__'

    def save(self, user):
        case = super(CadastroCaseForm, self).save(commit=False)
        case.criado_por = user
        try:
            case.save()
        except Exception as e:
            raise e
        return self