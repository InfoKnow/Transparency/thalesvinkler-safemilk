from django.conf.urls import url
from colortest import views

urlpatterns = [
    url(r'^colortestes/$', views.list_testes),
    url(r'^colortestes/(?P<pk>[0-9]+)/$', views.detail_teste),
]