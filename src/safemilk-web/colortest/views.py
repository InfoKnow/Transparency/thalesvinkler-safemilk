from django.shortcuts import render
from django.contrib.auth.models import User

from rest_framework import viewsets, status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from colortest.serializers import TesteSerializer, TestePostSerializer, UserSerializer
from colortest.models import Teste

from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
# from rest_framework.renderers import JSONRenderer
# from rest_framework.parsers import JSONParser

import uuid
from base64 import b64decode
from django.core.files.base import ContentFile  

# Create your views here.
# class JSONResponse(HttpResponse):
#     """
#     An HttpResponse that renders its content into JSON.
#     """
#     def __init__(self, data, **kwargs):
#         content = JSONRenderer().render(data)
#         kwargs['content_type'] = 'application/json'
#         super(JSONResponse, self).__init__(content, **kwargs)

@api_view(['GET', 'POST'])
def api_list_testes(request):
    """
    List all code testes, or create a new teste.
    """
    if request.method == 'GET':
        testes = Teste.objects.all()
        serializer = TesteSerializer(testes, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        # Convert base64 to img field
        image_data = b64decode(request.data['foto_base64'])
        image_name = str(uuid.uuid4())+".jpg"

        request.data['foto'] = ContentFile(image_data, image_name)

        serializer = TestePostSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def api_list_testes_by_user(request):
    """
    List all code testes, or create a new teste.
    """
    if request.method == 'POST':
        testes = Teste.objects.filter(usuario=request.data['id']).order_by('pk')
        # testes = Teste.objects.filter(usuario=1).order_by('pk')
        serializer = TesteSerializer(testes, many=True)
        return Response(serializer.data)


@api_view(['GET', 'PUT', 'DELETE'])
def api_detail_teste(request, pk):
    """
    Retrieve, update or delete a code teste.
    """
    try:
        teste = Teste.objects.get(pk=pk)
    except Teste.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = TesteSerializer(teste)
        return Response(serializer.data)

    elif request.method == 'PUT':
        # data = JSONParser().parse(request)
        serializer = TesteSerializer(teste, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        teste.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def api_list_usuarios(request):
    """
    List all users.
    """
    if request.method == 'POST':
        usuario = User.objects.filter(username=request.data['username']).order_by('pk')
        # users = Teste.objects.all()
        serializer = UserSerializer(usuario, many=True)
        return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



# API endpoint READ/UPDATE Teste
class TesteViewSet(viewsets.ModelViewSet):
	queryset = Teste.objects.all().order_by('data_hora')
	serializer_class = TesteSerializer

