from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.urls import reverse
from logistic.models import Ponto

# Create your models here.

class Cor(models.Model):
	COR_CHOICES = (
        ('Rosa', 'Rosa'),
        ('Roxo', 'Roxo'),
    )
	descricao = models.CharField(
        max_length=10,
        choices=COR_CHOICES,
    )

	def __str__(self):
		return self.descricao

	def get_absolute_url(self):
		return reverse('admin:list_cores')


class Teste(models.Model):
	RESULTADO_CHOICES = (
        ('Contaminado', 'Contaminado'),
        ('Seguro', 'Seguro'),
    )
	nome = models.CharField('Nome', max_length=45)
	data_hora = models.DateTimeField(default=timezone.now)
	resultado = models.CharField(
        max_length=11,
        choices=RESULTADO_CHOICES,
    )
	litros = models.IntegerField(default=0)
	foto = models.ImageField(upload_to='testes', blank=True, null=True)
	foto_base64 = models.TextField(blank=True, null=True)
	cor = models.ForeignKey(Cor, on_delete=models.CASCADE)
	usuario = models.ForeignKey(User, on_delete=models.CASCADE)
	ponto = models.ForeignKey(Ponto, on_delete=models.CASCADE)

	def __str__(self):
		return self.nome

	def get_absolute_url(self):
		return reverse('admin:list_testes')