# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-09 12:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('colortest', '0006_auto_20171009_1157'),
    ]

    operations = [
        migrations.AddField(
            model_name='teste',
            name='foto_base64',
            field=models.TextField(default='0'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='teste',
            name='foto',
            field=models.ImageField(blank=True, null=True, upload_to='testes'),
        ),
    ]
