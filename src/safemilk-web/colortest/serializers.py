from django.contrib.auth.models import User
from django.utils import timezone
from colortest.models import Teste, Cor
from rest_framework import serializers

from logistic.serializers import PontoSerializer

class CorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Cor
        fields = ('id', 'descricao')

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username')

class TesteSerializer(serializers.ModelSerializer):
	cor = CorSerializer(read_only=True)
	usuario = UserSerializer(read_only=True)
	ponto = PontoSerializer(read_only=True)
	
	class Meta:
		model = Teste
		fields = ('id','nome','data_hora','resultado','litros','foto', 'foto_base64','cor','usuario','ponto')

class TestePostSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Teste
		fields = ('id','nome','data_hora','resultado','litros','foto', 'foto_base64','cor','usuario','ponto')
