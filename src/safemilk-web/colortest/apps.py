from django.apps import AppConfig


class ColortestConfig(AppConfig):
    name = 'colortest'
