from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib.auth.models import User
from rest_framework import urls, routers, serializers, viewsets

from colortest.views import api_list_testes, api_list_testes_by_user, api_detail_teste, api_list_usuarios
from logistic.views import api_list_pontos, api_list_pontos_by_user

urlpatterns = [
    url(r'^admin/', include('admin.urls')),
    url(r'^', include('institucional.urls')),

    # url(r'^', include(router.urls)),


    # API - Testes
    url(r'^testes/$', api_list_testes),
    url(r'^testesbyuser/$', api_list_testes_by_user),
    url(r'^testes/(?P<pk>[0-9]+)/$', api_detail_teste),

    # API - User
    url(r'^usuarios/$', api_list_usuarios),

    # API - Pontos
    url(r'^pontos/$', api_list_pontos),
    url(r'^pontosbyuser/$', api_list_pontos_by_user),

    # Rest Auth
    url(r'^rest-auth/', include('rest_auth.urls')),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
