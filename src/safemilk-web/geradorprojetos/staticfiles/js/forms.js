// using jQuery to get CSRF token on cookie
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}


var csrftoken = getCookie('csrftoken');


$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

// END: CSRF TOKEN 

function show_errors(form, reasons){

    form.find('input, select, textarea, radio, checkbox').each(function(){
        $(this).parent('div').parent('div').removeClass('has-error');
        $(this).next().replaceWith("<span></span>");
    });
    
    $.each(reasons, function(key, value){
        $("[name="+key+"]").parent('div').parent('div').addClass('has-error');
        $("[name="+key+"]").next().replaceWith("<span id='"+key+"-error' class='help-block help-block-error'>"+value+"</span>");    
    });

}


function show_message(message){
    alert(message);
}

function send_form(method, action, formData, form){


    $.ajax({
        type        : method,
        url         : action, 
        data        : formData,
        dataType    : 'json',
        success     : function(data){

            if(data['new_html'] !== 'undefined'){
                form.html(data['new_html']);
                $.lastCall = (this.url);
            }
            if(data['html_replace_tag'] !== 'undefined'){
                $(data['html_replace_tag']).html(data['data']);
                $.lastCall = (this.url);
            }
            if(data['erros'] !== 'undefined'){
                $("form :input").each(function(){
                    $(this).closest('div').removeClass('has-error');
                    $(this).next("span.text-danger").html(" ");
                });

                $.each(data['erros'], function(key, value){
                    $("[name=" + "'" + key + "']").closest('div').addClass('has-error');
                    $("[name=" + "'" + key + "']").next("span.text-danger").html(value);
                });
            }
            if(data['hide_modal'] == 'undefined'){
                hide_modal();
            }

            if(data['after_url'] !== 'undefined'){
                $(location).attr('href',data['after_url']);                
            }

        },
        error       : function (xhr, ajaxOptions, thrownError) {
            $.lastCall = (this.url);
        }
    });

}


$(document).ready(function() {

    $('body').delegate('.form-submit', 'click', function(){ 
        var form =  $(this).closest('form');
        action = form.attr('data-form-action');
        method = form.attr('data-form-method');
        formData = form.serialize();
        send_form(method, action, formData, form);
    });

    $('body').delegate('.paginator', 'click', function(){
        var page = $(this).attr('data-page');
        var url = $.lastCall + '&page=' + page;
        send_ajax(url, 'GET', 'json');
    });

});


