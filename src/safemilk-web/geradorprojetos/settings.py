"""
Django settings for geradorprojetos project, on Heroku. For more info, see:
https://github.com/heroku/heroku-django-template

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/


SCRIPTS
    - Start/stop enviroment -> bin folder
        . activate
        deactivate
    - django-admin.py startproject nomedoprojeto

    - python manage.py runserver
    - python manage.py migrate
    - python manage.py startapp nomeapp

    - Change models in models.py
    - python manage.py makemigrations polls
    - python manage.py migrate
    
    - python manage.py sqlmigrate polls 0001

    - python manage.py createsuperuser




- registration: registro de empresas e funcionarios(vou poder usar isso depois pra outros roles);
- logistic: gerencia os percursos de cada empresa, os pontos de coleta de cada percurso, os caminhoes de cada linha, etc;
- test: gerencia testes feitos com suas respectivas fotos/cor;
- knowledge: tutoriais e videos demonstrativos de como realizar testes quimicos/fazer teste no app;
- notification: notificaoes gerais e também de aviso para compra de mais reagentes pela empresa;
- management: dashboards e reports gerencias


"""


import os
import dj_database_url


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
FILE_UPLOAD_MAX_MEMORY_SIZE = 5242880
FILE_UPLOAD_TEMP_DIR = "/tmp"


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: change this before deploying to production!
SECRET_KEY = 'i+acxn5(akgsn!sr4^qgf(^m&*@+g1@u^t@=8s@axc41ml*f=s'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Application definition

INSTALLED_APPS = (
    # 'jet.dashboard',
    # 'jet',
    # 'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'institucional.apps.InstitucionalConfig',
    'colortest.apps.ColortestConfig',
    'logistic.apps.LogisticConfig',
    'admin.apps.AdminConfig',
    'widget_tweaks',
    'django_extensions',
    'rest_framework',
    'rest_framework.authtoken',
    'corsheaders',
    'rest_auth'
)

# REST FRAMEWORK Settings
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        # 'rest_framework.permissions.IsAdminUser',
        # 'rest_framework.permissions.IsAuthenticatedOrReadOnly',
        'rest_framework.permissions.AllowAny',
    ],
    # 'PAGE_SIZE': 10,
    'DEFAULT_PAGINATION_CLASS': None,    
    'DEFAULT_AUTHENTICATION_CLASSES': (
        # 'rest_framework.authentication.BasicAuthentication',
        # 'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
}

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    # Simplified static file serving.
    # https://warehouse.python.org/project/whitenoise/
    'whitenoise.middleware.WhiteNoiseMiddleware',

)

ROOT_URLCONF = 'geradorprojetos.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': True,
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# # JET CONFIGURATIONS
# JET_DEFAULT_THEME = 'light-gray'
# JET_CHANGE_FORM_SIBLING_LINKS = False
# JET_SIDE_MENU_COMPACT = True
# JET_INDEX_DASHBOARD = 'institucional.dashboard.CustomIndexDashboard'
# JET_APP_INDEX_DASHBOARD = 'institucional.dashboard.CustomIndexDashboard'
# # END JET


WSGI_APPLICATION = 'geradorprojetos.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

#DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.postgresql_psycopg2',
#        'NAME': 'concentro',
#        'USER': 'icaro',
#        'PASSWORD': '12345',
#        'HOST': '127.0.0.1',
#        'PORT': '5432',
#    }
#}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'concentro',
        "USER": "thalesvinkler",
        "PASSWORD": "",
        "HOST": "localhost",
        "PORT": "",

        # 'USER': 'icaro',
        # 'PASSWORD': '12345',
        # 'HOST': '127.0.0.1',
        # 'PORT': '5432',
    }
}


# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'pt-BR'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Update database configuration with $DATABASE_URL.
db_from_env = dj_database_url.config(conn_max_age=500)
DATABASES['default'].update(db_from_env)

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = [
    '*',
    '127.0.0.1',
    '0.0.0.0:8102',
]

# CORS
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_METHODS = (
    'DELETE'
    'GET',
    'OPTIONS',
    'PATCH',
    'POST',
    'PUT',
)
CORS_ALLOW_HEADERS = (
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
)


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_ROOT = os.path.join(PROJECT_ROOT, 'staticfiles')
STATIC_URL = '/static/'

# Extra places for collectstatic to find static files.
STATICFILES_DIRS = (
    os.path.join(PROJECT_ROOT, 'static'),
)

# Simplified static file serving.
# https://warehouse.python.org/project/whitenoise/
# STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')


# Email config
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'thalesvinkler@gmail.com'
EMAIL_HOST_PASSWORD = 'Cudeflecha!0'

# Login Config
LOGIN_URL = 'admin:login'
LOGIN_REDIRECT_URL = 'admin:index'
