# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-23 13:27
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Caminhao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('placa', models.CharField(max_length=7)),
                ('chassi', models.CharField(max_length=21)),
                ('marca', models.CharField(max_length=21)),
                ('modelo', models.CharField(max_length=21)),
                ('cor', models.CharField(max_length=21)),
            ],
        ),
        migrations.CreateModel(
            name='Entreposto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=100)),
                ('cnpj', models.CharField(max_length=21)),
            ],
        ),
        migrations.CreateModel(
            name='Fazenda',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=100)),
                ('cnpj', models.CharField(max_length=21)),
                ('proprietario', models.CharField(max_length=45)),
            ],
        ),
        migrations.CreateModel(
            name='Laticinio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=100)),
                ('cnpj', models.CharField(max_length=21)),
                ('quant_reagente', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Linha',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=100)),
                ('origem', models.CharField(max_length=21)),
                ('destino', models.CharField(max_length=21)),
                ('ativa', models.BooleanField()),
                ('laticinio', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='logistic.Laticinio')),
            ],
        ),
        migrations.CreateModel(
            name='Local',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=45, verbose_name='Nome')),
                ('latitude', models.CharField(max_length=45)),
                ('longitude', models.CharField(max_length=45)),
                ('estado', models.CharField(max_length=45)),
                ('cidade', models.CharField(max_length=45)),
                ('endereco', models.CharField(max_length=45)),
            ],
        ),
        migrations.CreateModel(
            name='Ponto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_hora', models.DateTimeField(default=django.utils.timezone.now)),
                ('litros', models.IntegerField(default=0)),
                ('ordem', models.IntegerField(default=0)),
                ('foi_feito', models.BooleanField()),
                ('entreposto', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='logistic.Entreposto')),
                ('fazenda', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='logistic.Fazenda')),
                ('laticinio', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='logistic.Laticinio')),
            ],
        ),
        migrations.CreateModel(
            name='Processo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descricao', models.CharField(choices=[('1', 'Coleta'), ('2', 'Entrega')], max_length=7)),
            ],
        ),
        migrations.CreateModel(
            name='TipoPonto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descricao', models.CharField(choices=[('1', 'Entreposto'), ('2', 'Fazenda'), ('3', 'Laticínio')], max_length=10)),
            ],
        ),
        migrations.AddField(
            model_name='ponto',
            name='processo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='logistic.Processo'),
        ),
        migrations.AddField(
            model_name='ponto',
            name='tipoPonto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='logistic.TipoPonto'),
        ),
        migrations.AddField(
            model_name='linha',
            name='pontos',
            field=models.ManyToManyField(to='logistic.Ponto'),
        ),
        migrations.AddField(
            model_name='laticinio',
            name='local',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='logistic.Local'),
        ),
        migrations.AddField(
            model_name='fazenda',
            name='local',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='logistic.Local'),
        ),
        migrations.AddField(
            model_name='entreposto',
            name='local',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='logistic.Local'),
        ),
    ]
