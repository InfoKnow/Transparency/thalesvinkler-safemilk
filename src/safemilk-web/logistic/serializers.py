from django.contrib.auth.models import User
from django.utils import timezone
from logistic.models import Ponto, Linha, Fazenda, Laticinio, Entreposto, Processo, TipoPonto, Caminhao
from rest_framework import serializers


class FazendaSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Fazenda
		fields = ('__all__')

class LaticinioSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Laticinio
		fields = ('__all__')

class EntrepostoSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Entreposto
		fields = ('__all__')

class TipoPontoSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = TipoPonto
		fields = ('__all__')

class ProcessoSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Processo
		fields = ('__all__')

class CaminhaoSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Caminhao
		fields = ('__all__')

class LinhaSerializer(serializers.ModelSerializer):
	caminhao = CaminhaoSerializer(read_only=True)
	laticinio = LaticinioSerializer(read_only=True)
	
	class Meta:
		model = Linha
		fields = ('id', 'nome', 'origem', 'destino', 'ativa', 'laticinio', 'caminhao')

class PontoSerializer(serializers.ModelSerializer):
	# Nested Relationship
	linha = LinhaSerializer(read_only=True)
	fazenda = FazendaSerializer(read_only=True)
	laticinio = LaticinioSerializer(read_only=True)
	entreposto = EntrepostoSerializer(read_only=True)
	processo = ProcessoSerializer(read_only=True)
	tipoPonto = TipoPontoSerializer(read_only=True)

	class Meta:
		model = Ponto
		fields = ('id', 'ordem', 'linha', 'data', 'hora', 'litros', 'foi_feito', 'tipoPonto', 'processo', 'laticinio', 'entreposto', 'fazenda')
