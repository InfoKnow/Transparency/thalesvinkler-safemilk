from django.shortcuts import render

from rest_framework import viewsets, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.decorators import api_view

from logistic.serializers import PontoSerializer


from django.contrib.auth.models import User
from logistic.models import Ponto, Linha, Caminhao

# Create your views here.

@api_view(['GET'])
def api_list_pontos(request):
    """
    List all pontos.
    """
    if request.method == 'GET':
        pontos = Ponto.objects.all()
        serializer = PontoSerializer(pontos, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def api_list_pontos_by_user(request):
    """
    List all pontos by user.
    """
    if request.method == 'POST':

        caminhao = Caminhao.objects.filter(motorista=request.data['id']).order_by('pk')

        linhas = Linha.objects.filter(caminhao = caminhao.values('id')).order_by('pk')

        pontos = Ponto.objects.filter(linha = linhas.values_list('id')).order_by('pk')

        serializer = PontoSerializer(pontos, many=True)
        return Response(serializer.data)