from django.db import models
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.models import User
from admin.helper import validate_CPF, validate_CNPJ

# Create your models here.

class Local(models.Model):
	nome = models.CharField('Nome', max_length=45)
	latitude = models.CharField(max_length=45)
	longitude = models.CharField(max_length=45)
	estado = models.CharField(max_length=45)
	cidade = models.CharField(max_length=45)
	endereco = models.CharField(max_length=45)

	def __str__(self):
		return self.nome

	def get_absolute_url(self):
		return reverse('admin:list_locais')

class Laticinio(models.Model):
	nome = models.CharField(max_length=100)
	cnpj = models.CharField(unique=True, max_length=18, validators=[validate_CNPJ])
	quant_reagente = models.IntegerField(default=0)
	local = models.ForeignKey(Local, on_delete=models.CASCADE)
	proprietario = models.ForeignKey(User, on_delete=models.CASCADE)

	def __str__(self):
		return self.nome

	def get_absolute_url(self):
		return reverse('admin:list_laticinios')
		# return reverse('admin:update_laticinio', kwargs={'pk': self.pk})

class Entreposto(models.Model):
	nome = models.CharField(max_length=100)
	cnpj = models.CharField(unique=True, max_length=18, validators=[validate_CNPJ])
	local = models.ForeignKey(Local, on_delete=models.CASCADE)

	def __str__(self):
		return self.nome

	def get_absolute_url(self):
		return reverse('admin:list_entrepostos')

class Fazenda(models.Model):
	nome = models.CharField(max_length=100)
	cnpj = models.CharField(unique=True, max_length=18, validators=[validate_CNPJ])
	proprietario = models.CharField(max_length=45)
	local = models.ForeignKey(Local, on_delete=models.CASCADE)

	def __str__(self):
		return self.nome

	def get_absolute_url(self):
		return reverse('admin:list_fazendas')

class TipoPonto(models.Model):
	TIPO_PONTO_CHOICES = (
        ('1', 'Entreposto'),
        ('2', 'Fazenda'),
        ('3', 'Laticínio'),
    )
	descricao = models.CharField(
        max_length=10,
        choices=TIPO_PONTO_CHOICES,
    )

	def __str__(self):
		return self.descricao

class Processo(models.Model):
	PROCESSO_CHOICES = (
        ('1', 'Coleta'),
        ('2', 'Entrega'),
    )
	descricao = models.CharField(
		max_length=7,
		choices=PROCESSO_CHOICES,
	)

	def __str__(self):
		return self.descricao

class Caminhao(models.Model):
	placa = models.CharField(max_length=7)
	chassi = models.CharField(max_length=21)
	marca = models.CharField(max_length=21)
	modelo = models.CharField(max_length=21)
	cor = models.CharField(max_length=21)
	# linha = models.ManyToManyField(Linha)
	motorista = models.ForeignKey(User)

	def __str__(self):
		return self.placa

	def get_absolute_url(self):
		return reverse('admin:list_caminhoes')



class Linha(models.Model):
	nome = models.CharField(max_length=100)
	origem = models.CharField(max_length=21)
	destino = models.CharField(max_length=21)
	ativa = models.BooleanField()
	laticinio = models.ForeignKey(Laticinio, on_delete=models.CASCADE)
	# pontos = models.ManyToManyField(Ponto)
	caminhao = models.ForeignKey(Caminhao, on_delete=models.CASCADE)

	def __str__(self):
		return self.nome

	def get_absolute_url(self):
		return reverse('admin:list_linhas')

class Ponto(models.Model):
	ordem = models.IntegerField(default=0)
	linha = models.ForeignKey(Linha, on_delete=models.CASCADE)
	data = models.DateField()
	hora = models.TimeField(default=timezone.now)
	litros = models.IntegerField(default=0)
	foi_feito = models.BooleanField()
	tipoPonto = models.ForeignKey(TipoPonto, on_delete=models.CASCADE)
	processo = models.ForeignKey(Processo, on_delete=models.CASCADE)
	laticinio = models.ForeignKey(Laticinio, on_delete=models.CASCADE, blank=True, null=True)
	entreposto = models.ForeignKey(Entreposto, on_delete=models.CASCADE, blank=True, null=True)
	fazenda = models.ForeignKey(Fazenda, on_delete=models.CASCADE, blank=True, null=True)

	def __str__(self):
		return str(self.ordem)

	def get_absolute_url(self):
		return reverse('admin:list_pontos')



